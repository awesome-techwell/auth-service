def label = "worker-${UUID.randomUUID().toString()}"
def DOCKER_REPO = "localhost:32002"

podTemplate(label: label, containers: [
  containerTemplate(name: 'docker', image: 'docker', command: 'cat', ttyEnabled: true, envVars: [envVar(key: 'DOCKER_OPTS', value: '--insecure-registry <private ec2 ip>:30006')]),
  containerTemplate(name: 'nodejs', image: 'node:10.15', command: 'cat', ttyEnabled: true),
],
volumes: [
  hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock'),
  persistentVolumeClaim(mountPath: '/root/owasp-cve-db', claimName: 'jenkins-demo', readOnly: false)
])
 {
  node(label) {

    sh "env"

    container("nodejs") {
      env.JAVA_HOME="${tool 'default-jdk'}"
      echo "${env.JAVA_HOME}"
      env.PATH="${env.JAVA_HOME}/bin:${env.PATH}"
      sh "export PATH=${env.JAVA_HOME}/bin:${env.PATH}"
      sh "java -version"

      def myRepo = checkout scm

      stage('Install NPM dependencies') {
        sh "npm ci --quiet"
      }
      stage('Lint') {
        sh "npm run checkstyle"
      }
      stage('Test') {
        try {
          sh "npm run test"
          sh "npm run test-ci"
          sh "npm run test-cov"
        } catch (e) {
          throw e;
        } finally {
          publishHTML(target: [
            allowMissing         : false,
            alwaysLinkToLastBuild: true,
            keepAll              : true,
            reportDir            : 'reports',
            reportFiles          : 'coverage/lcov-report/index.html',
            reportName           : "Unit Tests"])
        }
      }

      stage("Third-Party Dependency Check") {
        def dependencyCheck = tool 'dependency-check'
        sh "${dependencyCheck}/bin/dependency-check.sh --project codeveros-auth-service --scan package-lock.json --format ALL --out . --data /root/owasp-cve-db"
        archiveArtifacts allowEmptyArchive: true, artifacts: 'dependency-check-report.*', onlyIfSuccessful: false
        dependencyCheckPublisher pattern: 'dependency-check-report.xml'
      }

      stage("SonarQube") {
        def scannerHome = tool 'sonarqube-scanner';
        withSonarQubeEnv('sonarqube') {
          sh "${scannerHome}/bin/sonar-scanner \
            -Dsonar.projectKey=codeveros-auth-service \
            -Dsonar.sources=lib,app.js -Dsonar.tests=test -Dsonar.language=js \
            -Dsonar.javascript.lcov.reportPaths=reports/coverage/lcov.info \
            -Dsonar.dependencyCheck.htmlReportPath=dependency-check-report.html \
            -Dsonar.dependencyCheck.reportPath=dependency-check-report.xml \
            -Dsonar.dependencyCheck.summarize=true"
        }
      }
    }

    container("docker") {
      docker.withRegistry("http://${DOCKER_REPO}", 'nexus') {
        stage('Build auth service') {
            echo "building docker image for auth service"
            def catalogImage = docker.build("${DOCKER_REPO}/authservice/authservice:latest")
            appImage = docker.build("${DOCKER_REPO}/authservice/authservice:latest")
            appImage.push()
        }
      }
    }
    // stage('trigger deployment') {
    //     build 'deployment'
    // }
  }
}
